# Bootstrap

An opinionated initialization of a mac from unboxing to dev ready in one cup of coffee.

```sh
bash <(curl -s https://gitlab.com/kamlasater/bootstrap/-/raw/master/init.sh) | tee "bootstrap-init-$(date +%Y%m%d-%H%M).log"
```
